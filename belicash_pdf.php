<?php
include "include\conecsi.inc";

//akhir koneksi
#ambil data di tabel dan masukkan ke array
$query = "SELECT * FROM vbelicash ORDER BY KodeCash";
$sql = mysql_query ($query);
$data = array();
while ($row = mysql_fetch_assoc($sql)) {
array_push($data, $row);
}
#setting judul laporan dan header tabel
$judul = "Laporan Data Beli Cash PT. Mentari Motor";
$header = array(
array("label"=>"Kode Cash", "length"=>23, "align"=>"L"),
array("label"=>"Tanggal Cash", "length"=>23, "align"=>"L"),
array("label"=>"Kode Cust", "length"=>23, "align"=>"L"),
array("label"=>"Nama", "length"=>23, "align"=>"L"),
array("label"=>"Alamat", "length"=>23, "align"=>"L"),
array("label"=>"Telepon", "length"=>23, "align"=>"L"),
array("label"=>"Kode Motor", "length"=>23, "align"=>"L"),
array("label"=>"Merk", "length"=>23, "align"=>"L"),
array("label"=>"Warna", "length"=>23, "align"=>"L"),
array("label"=>"Harga", "length"=>23, "align"=>"L"),
array("label"=>"Bayar", "length"=>23, "align"=>"L"),
array("label"=>"Keterangan", "length"=>23, "align"=>"L")
);
#sertakan library FPDF dan bentuk objek
include_once ("pdf/fpdf.php");
$pdf = new FPDF('L','mm','A4');
$pdf->AddPage('','');
#tampilkan judul laporan
$pdf->SetFont('Arial','B','15');
$pdf->Cell(0,20, $judul, '0', 1, 'C');
#buat header tabel
$pdf->SetFont('Arial','','7');
$pdf->SetFillColor('silver',0,0);
$pdf->SetTextColor(255);
$pdf->SetDrawColor('silver',0,0);
foreach ($header as $kolom) {
$pdf->Cell($kolom['length'], 5, $kolom['label'], 1, '0',
$kolom['align'], true);
}
$pdf->Ln();
#tampilkan data tabelnya
$pdf->SetFillColor(224,235,255);
$pdf->SetTextColor(0);
$pdf->SetFont('');
$fill=false;
foreach ($data as $baris) {
$i = 0;
foreach ($baris as $cell) {
$pdf->Cell($header[$i]['length'], 5, $cell, 1, '0',
$kolom['align'], $fill);
$i++;
}
$fill = !$fill;
$pdf->Ln();
}
#output file PDF
$pdf->Output();
?>