<script type="text/javascript" src="jquery-1.3.2.min.js"></script>
 <script type="text/javascript">
   $(document).ready(function(){
			$("#kodecust").change( function() {
	        $.ajax({
	            type: "POST",
	            data: "data_cust=" + $(this).val(),
	            url: "data.php",
	            success: function(msg){
	                if (msg != '') {
	                    $("#nama_cust").val(msg);
	                }
	            }
	        });
	    });
	
		$("#kodemotor").change( function() {
	        $.ajax({
	            type: "POST",
	            data: "data_motor=" + $(this).val(),
	            url: "data.php",
	            success: function(msg){
	                if (msg != ''){
	                    $("#merk").val(msg);
	                }
	            }
	        });
			
			$.ajax({
	            type: "POST",
	            data: "data_motor1=" + $(this).val(),
	            url: "data.php",
	            success: function(msg){
	                if (msg != ''){
	                    $("#warna").val(msg);
	                }
	            }
	        });
			
			$.ajax({
	            type: "POST",
	            data: "data_motor2=" + $(this).val(),
	            url: "data.php",
	            success: function(msg){
	                if (msg != ''){
	                    $("#harga").val(msg);
	                }
	            }
	        });
			
			$.ajax({
	            type: "POST",
	            data: "data_motor3=" + $(this).val(),
	            url: "data.php",
	            success: function(msg){
	                if (msg != ''){
	                    $("#harga").val(msg);
	                }
	            }
	        });
			
	    });
		
	});
	
	</script>

<script language="javascript" type="text/javascript">
  function validation() 
  {
    var pesan="";
	
	if (!document.getElementById('kodecash').value)
	{
	  pesan="Kode Cash harus diisi. \n";
	}
	if(!document.getElementById('tanggal').value)
	{
	  pesan="Tanggal harus diisi. \n";
	}
	
	if(!document.getElementById('alamat').value)
	{
	  pesan="Alamat harus diisi. \n";
	}
	
	if(!document.getElementById('telepon').value)
	{
	  pesan="Telepon harus diisi. \n";
	}
	
	if (pesan) 
	{
	  alert(pesan);
	  return false;  
	}else{
	  return true;
	}
	
  }
  
</script>		

<html>
<head>
<title>Untitled Document</title>
<style type="text/css">
<!--
.style1 {
	color: #000000;
	font-weight: bold;
}
-->
</style>
</head>

<body>
<?php
//ambil variabel untuk koneksi basis data
require("include/conecsi.inc");
mysql_select_db($database, $connect);

$simpan=$_POST['simpan'];
$kode_cash=$_GET['kode_cash'];

//cek apakah kondisi form terkirim atau tidak
if (!$simpan)
{
//jika form tidak dalam kondisi terkirim,
//tampilkan form pencarian nama
//cek apakah variabel $kode_brg dikirimkan
if (!$kode_cash) {
die ('Tidak ada data beli cash yang dipilih untuk diedit!'); }
//Tentukan query untuk ada yang akan diambil
$query="SELECT * FROM vbelicash WHERE KodeCash='$kode_cash'";
//jalankan query
$hasil = mysql_query($query) or die('Kesalahan Pada Proses Query!');
//cek dan ekstrak hasil query
$jml_rec = mysql_num_rows($hasil);
if (!($jml_rec>0)) { die('Data tidak ditemukan !');
}
list($kode_cash,$tanggal_cash,$kode_cust,$nama,$alamat,$telepon,$kode_motor,$merk,$warna,$harga,$bayar,$keterangan) = mysql_fetch_row($hasil);
?>
<center>
<form action="<?php echo $PHP_SELF ?>" method="post">
<p>
<table class="table_utama" border="0" cellspacing="2" cellpadding="2">
<tr>
<td class="td_headmenu" colspan="2" align="center">
  <p class="style1"><font size="3">Edit Data Beli Cash</font></p>
  <p class="style1">&nbsp;</p></td>
</tr>
<tr>
<td>Kode Cash</td>
<td><?php echo $kode_cash ?>
</td>
</tr>
<tr>
<td>Tanggal Cash</td>
<td>
<input name="form_tanggal" type="text" value="<?php echo $tanggal ?>" size="35" maxlength="40">
</td>
</tr>
<tr>
<td>Kode Cust</td>
<td>
<select name="form_kodecust" id="kodecust">
   <?php
         $sqldatapelanggan = mysql_query("SELECT * FROM pelanggan",$connect);
			while($hasil=mysql_fetch_array($sqldatapelanggan))
			{
			   echo "<option value='".$hasil['KodeCust']."'>".$hasil['KodeCust']." </option>";		
			}	
?>
  </select>
<input type="text" name="form_namacust" id="nama_cust" onFocus="true">
</td>

<tr>
<td>Alamat</td>
<td>
<textarea name="form_alamat" cols="35" rows="2"><?php echo $alamat ?></textarea>
</td>
</tr>
<tr>
<td>Telepon</td>
<td>
<input name="form_telepon" type="text" value="<?php echo $telepon?>" size="13" maxlength="15">
</td>
</tr>
<tr>
<td>Kode Motor</td>
<td>
<select name="form_kodemotor" id="kodemotor">
   <?php
			
			$minta = "SELECT KodeMotor FROM motor";
			$eksekusi = mysql_query($minta);
			while($hasil=mysql_fetch_array($eksekusi))
			{
			   echo "<option value='".$hasil['KodeMotor']."'>".$hasil['KodeMotor']." </option>";		
			}	
			?>
</select>
</td>
</tr>
<tr>
<td>Merk</td>
<td>
<input type="text" name="form_merk" size="25" value="<?php echo $merk ?>" maxlength="30">
</td>
</tr>
<tr>
<td>Warna</td>
<td>
<input type="text" name="form_warna" size="25" value="<?php echo $warna ?>" maxlength="30">
</td>
</tr>
<tr>
<td>Harga</td>
<td>
<input type="text" name="form_harga" size="25" value="<?php echo $harga ?>" maxlength="30">
</td>
</tr>
<tr>
<td>Bayar</td>
<td>
<input type="text" name="form_bayar" size="25" value="<?php echo $bayar ?>" maxlength="30">
</td>
</tr>
<tr>
<td>Keterangan</td>
<td>
<textarea name="form_keterangan" cols="35" rows="2"><?php echo $keterangan ?></textarea>
</td>
</tr>
<tr>
<td colspan="2" align="center">
<input type="submit" name="simpan" value="Update">
</td>
</tr>
</table>
</form>
</center><br /><br /><br /><br />
<?php
//bebaskan memori yang digunakan untuk proses
mysql_free_result($hasil);
}
else
{
//jika form dalam kondisi terkirim
//lakukan perubahan basis data
//tentukan query 

$form_kodecash=$_POST['form_kodecash'];
$form_tanggal=$_POST['form_tanggal'];
$form_kodecust=$_POST['form_kodecust'];
$form_nama=$_POST['form_nama'];
$form_alamat=$_POST['form_alamat'];
$form_telepon=$_POST['form_telepon'];
$form_kodemotor=$_POST['form_kodemotor'];
$form_merk=$_POST['form_merk'];
$form_warna=$_POST['form_warna'];
$form_harga=$_POST['form_harga'];
$form_bayar=$_POST['form_bayar'];
$form_keterangan=$_POST['form_keterangan'];

$query = "UPDATE belicash SET
KodeCash='".addslashes($form_kodecash)."',
TanggalCash='".addslashes($form_tanggal)."',
KodeCust='".addslashes($form_kodecust)."',
Nama='".addslashes($form_nama)."',
Alamat='".addslashes($form_alamat)."',
Telepon='".addslashes($form_telepon)."',
KodeMotor='".addslashes($form_kodemotor)."',
Merk='".addslashes($form_merk)."',
Warna='".addslashes($form_warna)."',
Harga=".addslashes($form_harga).",
Bayar=".addslashes($form_bayar).",
Keterangan='".addslashes($form_keterangan)."',
WHERE KodeCash='$form_kodecash'";
//lakukan proses query
$hasil = mysql_db_query($database,$query,$connect) or die('Kesalahan Pada Proses Query!');
//Tampilkan pesan proses edit telah selesai
?>
<center>
<font size="4" color="black">Proses Edit Berhasil</font><p>
Data Beli Cash
<b><?php echo addslashes ($form_kodecash) ?></b>
telah disimpan perubahannya.
<p>
<a href="?act=belicash">Klik disini untuk kembali</a></p></p></center>
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<?php
}
?>
</body>
</html>