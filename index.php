<?php
include("include/conecsi.inc");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Aplikasi Kredit Motor</title>
    <link rel="shortcut icon" href="images/product_06.png">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style.css" />
<link rel="stylesheet" type="text/css" href="styles/styles.css" />
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
<link href="signin/signin.css" rel="stylesheet">
<script language="javascript" type="text/javascript" src="scripts/mootools-1.2.1-core.js"></script>
<script language="javascript" type="text/javascript" src="scripts/mootools-1.2-more.js"></script>
<script language="javascript" type="text/javascript" src="scripts/slideitmoo-1.1.js"></script>
<script language="javascript" type="text/javascript">
window.addEvents({
    'domready': function () { /* thumbnails example , div containers */
        new SlideItMoo({
            overallContainer: 'SlideItMoo_outer',
            elementScrolled: 'SlideItMoo_inner',
            thumbsContainer: 'SlideItMoo_items',
            itemsVisible: 5,
            elemsSlide: 3,
            duration: 300,
            itemsSelector: '.SlideItMoo_element',
            itemWidth: 158,
            showControls: 1
        });
    },

});

function clearText(field) {
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}
</script>
<style type="text/css">
<!--
.style6 {font-family: "Bauhaus 93"; font-size: 24px; color: #000000;}
.style7 {
	color: #FFFFFF;
	font-family: Andalus;
	font-weight: bold;
}
-->
</style>
</head>
<body>

<?php
if (!(isset($_SESSION['user_id']))) {
?>

<p><? 
if (isset($_GET['message'])) {
echo $_GET['message']; }?></p>
<form id="form1" name="form1" method="post" action="login.php">
 <div id="site_title" style="position:relative;left:110px;">
      <h1><a href="?"><img src="images/logo.png" alt="" /></a></h1>
    </div>
<div class="container" style="position:relative;top:30px">
      <form class="form-signin" role="form">
        <h4 class="form-signin-heading">Please Log In !!</h4>
        <input type="text" class="form-control" name="username" style="width:300px" id="username" placeholder="Your Username" required autofocus>
        <input type="password" class="form-control" name="password" style="width:300px" id="password" placeholder="Your Password" required>
		      <label>
		 <input name="Login" type="submit" id="Login" value="Masuk" class="btn btn-lg btn-primary" style="width:300px;font-size:14px" />
		       </label>
         </form>

    </div>
</form>
<?php

}else{

?>

<div id="site_title_bar_wrapper">
  <div id="site_title_bar">
    <div id="site_title">
      <h1><a href="?"><img src="images/logo.png" alt="" /></a></h1>
    </div>
    <div id="search_box">
      <form action="#" method="get">
        <input type="text" value="Masukkan Kata Kunci" name="q" size="10" id="searchfield" onFocus="clearText(this)" onBlur="clearText(this)" />
        <input type="submit" name="Search" value="" alt="Search" id="searchbutton" />
      </form>
    </div>
    <div id="menu">
      <ul>
        <li><a href="?">Home</a></li>
        <li><a href="?act=motor">Data Motor </a></li>
        <li><a href="?act=pelanggan">Data Pelanggan </a></li>
        <li><a href="?act=belicash">Data Beli Cash </a></li>
        <li><a href="?act=belikredit">Data Beli Kredit </a></li>
        <li><a href="?act=bayarcicilan">Data Bayar Cicilan </a></li>
		<li><a href="?act=user">Data User </a></li>
        <li><a href="belicash_pdf.php" target="_blank">Laporan Transaksi Beli Cash</a></li>
        <li><a href="belikredit_pdf.php" target="_blank">Laporan Transaksi Beli Kredit</a></li>
        <li><a href="#" target="_blank">Laporan Transaksi Bayar Cicilan</a></li>
		<li><a href="logout.php" onClick="return confirm('Anda yakin keluar')">Logout </a></li>
      </ul>
    </div>
    <!-- end of menu -->
  </div>
</div>
<div id="content">
  <div id="product_gallery">
    <div id="SlideItMoo_outer">
      <div id="SlideItMoo_inner">
        <div id="SlideItMoo_items">
          <div class="SlideItMoo_element"> <strong>Bajaj 180 CC</strong> <a href="#"> <img src="images/product_01.png" alt="" /></a> </div>
          <div class="SlideItMoo_element"> <strong>Supra Fit 125</strong> <a href="#"> <img src="images/product_02.png" alt="" /></a> </div>
          <div class="SlideItMoo_element"> <strong>Tiger 2011</strong> <a href="#"> <img src="images/product_03.png" alt="" /></a> </div>
          <div class="SlideItMoo_element"> <strong>Jupiter MX</strong> <a href="#"> <img src="images/product_04.png" alt="" /></a> </div>
          <div class="SlideItMoo_element"> <strong>Bison</strong> <a href="#"> <img src="images/product_05.png" alt="" /></a> </div>
          <div class="SlideItMoo_element"> <strong>Titan</strong> <a href="#"> <img src="images/product_06.png" alt="" /></a> </div>
          <div class="SlideItMoo_element"> <strong>Vixion</strong> <a href="#"> <img src="images/product_07.png" alt="" /></a> </div>
          <div class="SlideItMoo_element"> <strong></strong> <a href="#"> <img src="images/product_08.png" alt="" /></a> </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end of product gallery -->
  <div class="section_w940">
    <?php include "buka_halaman.php";
	?>
  </div>
<?php
}
?>
 
</body>
</html>
